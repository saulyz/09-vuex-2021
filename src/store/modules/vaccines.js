export default {
  namespaced: true,

  state: {
    vaccines: [
      { id: 'pfizer', name: 'Pfizer–BioNTech', quantity: 1 },
      { id: 'astrazeneca', name: 'Oxford–AstraZeneca', quantity: 1},
      { id: 'johnsonandjohnson', name: 'Johnson & Johnson\'s Janssen', quantity: 1}
    ],
  },

  getters: {
    getVaccineName: (state) => (id) => {
      const [result] = state.vaccines.filter(vaccine => vaccine.id === id)
        .map(vaccine => vaccine.name);
      return result;
    }
  },

  mutations: {},

  actions: {},
};